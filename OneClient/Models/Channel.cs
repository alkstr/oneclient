using System.Collections.Generic;
using OneClient.Services;

namespace OneClient.Models;

public class Channel
{
    public string Name { get; set; } = string.Empty;

    public static IEnumerable<Channel>? Request()
    {
        var content = new Dictionary<object, object>();
        var result = API.Post<IEnumerable<Channel>>("/chat/get-channels", content);
        return result;
    }
}