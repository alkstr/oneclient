using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.VisualBasic;
using OneClient.Services;

namespace OneClient.Models;

public class Message
{
    public string User { get; set; } = string.Empty;
    public string Text { get; set; } = string.Empty;
    public long Time { get; set; }
    public string DisplayTime => TimeZoneInfo.ConvertTime(
        DateTimeOffset.FromUnixTimeSeconds(Time), 
        TimeZoneInfo.Local
        ).ToString();

    public static IEnumerable<Message>? Request(Channel channel)
    {
        var content = new Dictionary<object, object> { { "Channel", channel.Name } };
        var result = API.Post<IEnumerable<Message>>("/chat/get-messages", content);
        return result;
    }
}