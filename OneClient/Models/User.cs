using System;
using System.Collections.Generic;
using System.Linq;
using OneClient.Services;

namespace OneClient.Models;

public class User
{
    public string Name { get; set; }
    public bool Online { get; set; }

    public static IEnumerable<User> Request()
    {
        var content = new Dictionary<object, object> { { "Token", API.Token } };
        var result = API.Post<IEnumerable<User>>("/auth/get-online", content);
        
        IComparer<User> comparer = new Comparer();
        return result!.OrderByDescending(u => u, comparer);
    }

    private class Comparer : IComparer<User>
    {
        public int Compare(User a, User b)
        {
            var result = a.Online.CompareTo(b.Online);
            if (result == 0) result = string.Compare(b.Name, a.Name, StringComparison.Ordinal);

            return result;
        }
    }
}