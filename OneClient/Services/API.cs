using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;

namespace OneClient.Services;

// ReSharper disable once InconsistentNaming
public static class API
{
    private static readonly HttpClient HttpClient = new();
    public static string Hostname { get; set; } = string.Empty;
    public static string Token { get; set; } = string.Empty;

    public static Dictionary<string, object>? Post(string path, Dictionary<object, object> content) =>
        Post<Dictionary<string, object>?>(path, content);

    public static T? Post<T>(string path, Dictionary<object, object> content)
    {
        var responseContent = Request(path, content);
        try
        {
            var result = JsonSerializer.Deserialize<T?>(responseContent);
            return result;
        }
        catch (JsonException e)
        {
            Console.WriteLine(e);
            return default;
        }
        
    }

    private static string Request(string path, Dictionary<object, object> content)
    {
        var jsonContent = JsonSerializer.Serialize(content);
        try
        {
            var response = HttpClient
                .PostAsync(
                    Hostname + path,
                    new StringContent(jsonContent, Encoding.UTF8, "application/json")
                ).Result;
            return response.Content.ReadAsStringAsync().Result;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return "";
        }
    }
}