using System.Collections.Generic;
using OneClient.Services;

namespace OneClient.ViewModels;

public class Auth : ViewModelBase
{
    public string Hostname { get; set; } = "http://localhost:8100";
    public string Username { get; set; } = "admin";
    public string Password { get; set; } = "admin";

    public void SignIn()
    {
        API.Hostname = Hostname;
        var json = new Dictionary<object, object> { { "Name", Username }, { "Password", Password } };
        var response = API.Post("/auth/get-token", json) ?? new Dictionary<string, object>();

        if (response.TryGetValue("Token", out var token))
        {
            API.Token = token.ToString() ?? string.Empty;
        }
    }
}