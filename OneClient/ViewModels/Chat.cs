using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Avalonia.Threading;
using DynamicData;
using OneClient.Models;
using OneClient.Services;
using ReactiveUI;

namespace OneClient.ViewModels;

// ReSharper disable once InconsistentNaming
public class Chat : ViewModelBase
{
    public Chat()
    {
        Channels = new ObservableCollection<Channel>(Channel.Request() ?? Array.Empty<Channel>());
        _currentChannel = Channels.FirstOrDefault();
        
        MessageHistory = _currentChannel is null ? 
            new ObservableCollection<Message>(Array.Empty<Message>()) : 
            new ObservableCollection<Message>(Message.Request(_currentChannel) ?? Array.Empty<Message>());
        
        _messageHistoryTimer = new DispatcherTimer(TimeSpan.FromSeconds(1), DispatcherPriority.Normal, (_, _) =>
        {
            MessageHistory.Clear();
            if (_currentChannel is null)
            {
                return;
            }
            MessageHistory.Add(Message.Request(_currentChannel) ?? Array.Empty<Message>());
        });
        _messageHistoryTimer.Start();

        Users = new ObservableCollection<User>(User.Request() ?? Array.Empty<User>());
        _usersTimer = new DispatcherTimer(TimeSpan.FromSeconds(5), DispatcherPriority.Normal, (_, _) =>
        {
            Users.Clear();
            Users.Add(User.Request() ?? Array.Empty<User>());
        });
        _usersTimer.Start();
    }

    //
    // CHANNELS
    //

    public ObservableCollection<Channel> Channels { get; }

    private Channel? _currentChannel;
    
    public void ChangeChannelWithMessages(Channel channel)
    {
        _currentChannel = channel;
        MessageHistory.Clear();
        MessageHistory.Add(Message.Request(_currentChannel) ?? Array.Empty<Message>());
    }

    public void RemoveChannel(string name)
    {
        API.Post("/chat/remove-channel", new Dictionary<object, object> { { "Name", name } });
        Channels.Clear();
        Channels.Add(Channel.Request() ?? Array.Empty<Channel>());
        if (_currentChannel is not null && _currentChannel.Name == name)
        {
            _currentChannel = Channels.FirstOrDefault();
        }
    }
    
    public void CreateChannel(string name)
    {
        API.Post("/chat/create-channel", new Dictionary<object, object> { { "Name", name } });
        Channels.Clear();
        Channels.Add(Channel.Request() ?? Array.Empty<Channel>());
    }

    //
    // MESSAGE HISTORY
    //

    public ObservableCollection<Message> MessageHistory { get; }

    private readonly DispatcherTimer _messageHistoryTimer;

    //
    // MESSAGE FIELD
    //

    public string MessageField
    {
        get => _messageField;
        set => _messageField = this.RaiseAndSetIfChanged(ref _messageField, value);
    }

    private string _messageField = string.Empty;
    
    public void SendMessage()
    {
        if (_currentChannel is null) return;
        if (string.IsNullOrWhiteSpace(MessageField)) return;
        
        var content = new Dictionary<object, object>
        {
            { "Token", API.Token },
            { "Text", MessageField },
            { "Channel", _currentChannel.Name }
        };
        API.Post("/chat/send-message", content);
        MessageField = string.Empty;
    }

    //
    // USERS
    //

    public ObservableCollection<User> Users { get; }

    private readonly DispatcherTimer _usersTimer;
}