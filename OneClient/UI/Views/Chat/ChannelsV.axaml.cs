using Avalonia.Controls;
using Avalonia.Interactivity;
using OneClient.Models;

namespace OneClient.UI.Views.Chat;

public partial class ChannelsV : UserControl
{
    public ChannelsV()
    {
        InitializeComponent();
    }

    // TODO: cursed but works
    private void ChangeChannel(object? sender, RoutedEventArgs e)
    {
        var button = sender as Button;
        var dataContext = (OneClient.ViewModels.Chat)DataContext!;
        dataContext.ChangeChannelWithMessages(new Channel { Name = (button!.Content as string)! });
    }
}