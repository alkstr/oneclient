using Avalonia.Controls;
using Avalonia.Interactivity;
using OneClient.UI.Windows;

namespace OneClient.UI.Views.Chat;

public partial class TopBarV : UserControl
{
    public TopBarV()
    {
        InitializeComponent();
    }

    private void OpenSettings(object sender, RoutedEventArgs e)
    {
        var cw = new SettingsW();
        cw.Show();
        cw.DataContext = DataContext;
    }
}