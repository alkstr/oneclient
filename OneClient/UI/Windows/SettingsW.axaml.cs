using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using OneClient.Models;
using OneClient.ViewModels;

namespace OneClient.UI.Windows;

public partial class SettingsW : Window
{
    public SettingsW()
    {
        InitializeComponent();
    }

    private void RemoveChannel(object? sender, RoutedEventArgs e)
    {
        if (ChannelsList.SelectedItem is null)
        {
            return;
        }
        var name = ((Channel)ChannelsList.SelectedItem).Name;
        var dataContext = (Chat)DataContext!;
        dataContext.RemoveChannel(name);
    }
    
    private void CreateChannel(object? sender, RoutedEventArgs e)
    {
        var name = CreateChannelName.Text ?? "";
        var dataContext = (Chat)DataContext!;
        dataContext.CreateChannel(name);
    }
}