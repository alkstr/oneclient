using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Interactivity;
using OneClient.Services;
using OneClient.ViewModels;

namespace OneClient.UI.Windows;

public partial class AuthW : Window
{
    public AuthW()
    {
        DataContext = new Auth();
        InitializeComponent();
    }

    private void SignIn(object sender, RoutedEventArgs e)
    {
        ((Auth)DataContext!).SignIn();
        if (string.IsNullOrEmpty(API.Token))
        {
            var control = (Control)sender;
            FlyoutBase.ShowAttachedFlyout(control);
            var flyout = (Flyout)FlyoutBase.GetAttachedFlyout(control)!;
            flyout.Content = "Can't connect to the server";
            return;
        }

        var cw = new ChatW();
        cw.Show();
        Close();
    }
}