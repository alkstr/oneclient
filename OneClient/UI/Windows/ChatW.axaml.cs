using Avalonia.Controls;
using OneClient.ViewModels;

namespace OneClient.UI.Windows;

public partial class ChatW : Window
{
    public ChatW()
    {
        DataContext = new Chat();
        InitializeComponent();
    }
}